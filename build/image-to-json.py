import os
import json
from PIL import Image

IMG_DIR = 'img'


def get_full_path(*args):
    return os.path.join(*args)


def get_file_info(dir_name):
    file_list_directory = os.listdir(get_full_path(IMG_DIR, dir_name))
    local_data = list()
    for file_name in file_list_directory:
        img = Image.open(get_full_path(IMG_DIR, dir_name, file_name))
        local_data.append({'path': IMG_DIR + '/' + dir_name + '/',
                            'mode': img.mode,
                            'format': img.format,
                            'size': img.size,
                            'short_name': file_name.split('.')[0],
                            'full_name': file_name})
    return {dir_name: local_data}

def main():
    cwd = os.getcwd()
    dir_list_directory = os.listdir(get_full_path(cwd, IMG_DIR))
    print('All items in directory: {}'.format(dir_list_directory))

    data = dict()
    for directory in dir_list_directory:
        if not os.path.isdir(get_full_path(cwd, IMG_DIR, directory)):
            print(directory, 'is not directory')
            continue
        data.update(get_file_info(directory))

        with open('files.json', 'w') as f:
            json.dump(data, f)


if __name__ == '__main__':
    main()
# 
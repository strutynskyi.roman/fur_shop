// Main javascript entry point
// Should handle bootstrapping/starting application

'use strict';
// ------------------------------
// Imports
// ------------------------------

import $ from 'jquery';
// export for others scripts to use
// require('jquery-ui/ui/core')
// require('jquery-ui/ui/widget')
// require('jquery-ui/ui/effect')
window.$ = window.jQuery = $;

require('jquery.waterwheelCarousel/js/jquery.waterwheelCarousel.js');
// require('allinone_carousel/js/jquery.touchSwipe.min.js');

// Modules










// import Link from './../_modules/molecules/menu/menu';

// Organisms
import Header from './../_modules/organisms/header/header'  
//Molecules 
import Slider from './../_modules/molecules/slider/slider'
import ImgList from './../_modules/molecules/img-list/img-list'
import Sewing from './../_modules/molecules/sewing/sewing'

//Atoms
import Hamburger from './../_modules/atoms/hamburger/hamburger'

// ------------------------------
// Additional functionality
// ------------------------------

// Crossbrowser plugin for icons
import svg4everybody from 'svg4everybody/dist/svg4everybody.legacy.js';
svg4everybody();

// Data pages
let currentPage = $('body').data('page');

// ------------------------------
// Initialization modules
// ------------------------------

$(() => {
  // Common scripts

  // Home page scripts
  new Header();
  new Slider();
  new Sewing();
  new Hamburger();
  
  new ImgList();
  // new Tabs();
  // new Rangle();
  // if (currentPage === pages.home.name) {
  //   new Link();
  // }
});

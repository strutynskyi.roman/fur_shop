'use strict';
require('bootstrap-autohide-navbar/dist/bootstrap-autohide-navbar.js')
export default class Header {
  constructor() {
    this.headerSelector = '.header';
    this.initSelector();
  }
  initSelector(){


      
      let currentPage = $('body').data('page');
      
      if (currentPage == 'home') {
          $('a.page-scroll').bind('click', function(event) {
              var self = $(this);
              if ($('a.page-scroll').hasClass('menu__item--active')) {
                  $('a.page-scroll').removeClass('menu__item--active');
                  self.addClass('menu__item--active');
              } else {
                  self.addClass('menu__item--active');
              }
              $('html, body').stop().animate({
                  scrollTop: $(self.attr('href')).offset().top - 100 
              }, 500,function (){
                  if (self.attr('href') != '#main__home') {
                      $(".header").css({background:'white',
                      transition:'.5s'});
                      } else {
                          $(".header").css({background:'rgba(255, 255, 255, 0.40)',
                      transition:'.5s'});
                  }
              });   
              event.preventDefault();
          });
        $(window).scroll(function(){
            var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
            var div_top = $('.header').offset().top;
                if ($(window).width() > "768" && $(".header").offset().top > 980) {
                $(".header").css({background:'white',
                    transition:'.5s'});
                }else if ($(window).width() <= "768" && $(".header").offset().top > 480) {
                    $(".header").css({background:'white',
                    transition:'.5s'});
                }else if ($(window).width() <= "425" && $(".header").offset().top > 280) {
                    $(".header").css({background:'white',
                    transition:'.5s'});
                }else{
                    $(".header").css({background:'rgba(255, 255, 255, 0.40)',
                    transition:'.5s'});
                }
            });
        var aChildren = $(".menu a"); 
        var aArray = []; 
        for (var i=0; i < aChildren.length; i++) {    
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        } 

        $(window).scroll(function(){
            var windowPos = $(window).scrollTop(); 
            var windowHeight = $(window).height(); 
            var docHeight = $(document).height();

            for (var i=0; i < aArray.length; i++) {
                var theID = aArray[i];
                var divPos = $(theID).offset().top - 100; 
                var divHeight = $(theID).height(); 
                if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                    $("a[href='" + theID + "']").addClass("menu__item--active");
                } else {
                    $("a[href='" + theID + "']").removeClass("menu__item--active");
                }
            }
            if(windowPos + windowHeight == docHeight) {
                if (!$(".menu a:last-child ").hasClass("menu__item--active")) {
                    var navActiveCurrent = $(".menu__item--active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("menu__item--active");
                    $(".menu a:last-child").addClass("menu__item--active");
                }
            }
        });
    }
    if (currentPage == 'news') {
        $(window).width() > "768" && $(".header").bootstrapAutoHideNavbar();    
    }    
    if (currentPage == 'new') {
        $(window).width() > "768" && $(".header").bootstrapAutoHideNavbar();
    }    
  }
}

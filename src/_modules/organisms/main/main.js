'use strict';

export default class Main {
  constructor() {
    this.name = 'main';
    console.log('%s module', this.name.toLowerCase());
  }
}

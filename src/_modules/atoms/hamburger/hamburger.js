
'use strict';
import Menu from '../../molecules/menu/menu';

export default class Hamburger extends Menu{
  constructor() {
    super();
    this.hamburgerSelector = '.js-hamburger';
    this.initHamburger();
  }

  initHamburger(){
     $(this.hamburgerSelector).click(() => {
        this.menuOpen();
      });
  }
}
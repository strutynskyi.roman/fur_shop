'use strict';

export default class Menu {
  constructor() {
    this.menuSelector = '.js-menu';
    this.headerSelector = '.header';
    this.overlaySelector = '.js-overlay';
    this.exitSelector = '.js-exit'
    this.init();
  }
  init(){
    $(this.exitSelector).click(() => this.menuExit());
    $(this.overlaySelector).click(() => this.menuExit());
  }
  menuOpen() {
    $(this.menuSelector).addClass('menu__open');
    $(this.overlaySelector).addClass('menu__open');
    $(this.headerSelector).addClass('header--open');
    
  }
  menuExit() {
    $(this.menuSelector).removeClass('menu__open');
    $(this.overlaySelector).removeClass('menu__open');
    $(this.headerSelector).removeClass('header--open');
    
  }
}
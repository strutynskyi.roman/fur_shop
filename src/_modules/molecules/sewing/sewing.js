'use strict';

export default class Sewing {
  constructor() {
  let self = this;       
    this.sewingCloseSelector = '.js-sewingClose';
    this.sewingOverlay = '.sewing__overlay';
    this.sewingSelector = '.sewing';
    this.sewingOpen = '.js-openSeving';
    
   
    this.initSewing();  
  }
  initSewing(){
    $(this.sewingOpen).click(() => this.openSewing());
    $(this.sewingCloseSelector).click(() => this.closeSewing());
    $(this.sewingOverlay).click(() =>  this.closeSewing());
     
  
  }
  openSewing() {
    $(this.sewingSelector).addClass('open');
    $(this.sewingOverlay).addClass('open');

  }
  closeSewing() {
    $(this.sewingSelector).removeClass('open');
    $(this.sewingOverlay).removeClass('open');
   
    
  }
}
